#!/bin/bash
#timer
SECONDS=0

#colors
red=`tput setaf 1`
green=`tput setaf 2`
white=`tput setaf 6`
reset=`tput sgr0`

#clear
clear

#hostname
STATUS=1
while [ $STATUS -ne 0 ]; do
    echo "${white}[?] What will be the name of this computer?${reset}"
    read NAME
    if [ ! -z $NAME ]; then
      STATUS=0
      break
    else
      echo "${red}["\!"]Invalid hostname"\!"${reset}"
    fi
done

#username
STATUS=1
while [ $STATUS -ne 0 ]; do
    echo "${white}[?] What will be the USERNAME?${reset}"
    read USERNAME
    if [ ! -z $USERNAME ]; then
      STATUS=0
      break
    else
      echo "${red}["\!"] Invalid hostname"\!"${reset}"
    fi
done

echo "${white}[?] UEFI or legacy boot? (legacy is the default)${reset}"
read BOOTTYPE

if [[ "$BOOTTYPE" != "uefi" || "$BOOTTYPE" != "UEFI" ]]; then
	echo "${red}["\!"] Unsupported mode, legacy will be used.${reset}"
	BOOTTYPE="legacy"
fi

if [ ! -f /sys/firmware/efi/efivars ]; then	
	if [ "$BOOTTYPE" != "legacy" ]; then
		echo "${red}["\!"] UEFI is not supported will use legacy boot!${reset}"
		BOOTTYPE="legacy"
	fi
fi

#partitioning
echo "${green}[-] Start Formating....(Will need a /boot/ -> 500M EFI (if UEFI)/linux (if legacy)  and / -> 20+GB )${reset}"
sleep 3
cfdisk

#Get the partitions
STATUS=1
while [ $STATUS -ne 0 ]; do
  #list partitions
  echo "${green}[|] Partitions${reset}"
  fdisk -l | grep /dev | sed '1d'
  if [ "$BOOT" == "" ]; then
    #read boot partition
    echo "${white}[?] What's the name of your boot partition (Format: /dev/sda?)?${reset}"
    read BOOT
    #check if the partition exists
    TESTIT=`fdisk -l | grep $BOOT`
    if [ "$TESTIT" == "" ]; then
      echo "${red}["\!"] $BOOT partition not found"\!"${reset}"
      BOOT=""
      continue
    fi
    #is it seems correct (not sure if it's)
    LASTCHAR=`echo "${BOOT: -1}"`
    if [[ "$LASTCHAR" != [0-9] ]]; then
      echo "${red}["\!"] Invalid partition: $BOOT${reset}"
      BOOT=""
      continue
    fi
  fi
  #read root partition
  echo "${white}[?] What's the name of your root partition (Format: /dev/sda?)?${reset}"
  read HOMEDIR
  #check if the partition exists
  TESTIT=`fdisk -l | grep $HOMEDIR`
  if [ "$TESTIT" == "" ]; then
    echo "${red}["\!"] $HOMEDIR partition not found"\!"${reset}"
    continue
  fi
  #is it seems correct (not sure if it's)
  LASTCHAR=`echo "${HOMEDIR: -1}"`
  if [[ "$LASTCHAR" != [0-9] ]]; then
    echo "${red}["\!"] Invalid partition: $HOME${reset}"
    continue
  fi
  #check if the 2 dirs not the same (if the same, starting the loop from the beginnig)
  if [ "$BOOT" == "$HOMEDIR" ]; then
    echo  "${red}["\!"] Boot and root partition can not be the same"\!"${reset}"
    BOOT=""
    HOMEDIR=""
    continue
  fi
  #if everything seems good
  if [[ "$BOOT" != "" && "$HOMEDIR" != "" && "$BOOT" != "$HOMEDIR" ]]; then
    #confirm
    echo  "${red}[-] BOOT: ${green}$BOOT${reset}"
    echo "${red}[-] HOME: ${green}$HOMEDIR${reset}"
    read -p "${white}[[?]] Is it ok? (write ${red}yes${white})${reset}" AWS
    if [ "$AWS" == "yes" ]; then
      STATUS=0
      break
    else
      #if not good
      BOOT=""
      HOMEDIR=""
      continue
    fi
  fi
done

#Encryption
echo "${green}[-] Encypting partitions ${reset}"
cryptsetup --verbose --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 5000 --use-random luksFormat $HOMEDIR
cryptsetup open --type luks $HOMEDIR cryptroot

#Formating
echo "${green}[-] Formating partitions ${reset}"
if [ "$BOOTTYPE" == "legacy" ]; then
	mkfs.ext4 $BOOT
else
	mkfs.fat -F32 $BOOT
fi

mkfs.ext4 /dev/mapper/cryptroot

#Mounting
echo "${green}[-] Mounting partitions ${reset}"
mount /dev/mapper/cryptroot /mnt
if [ "$BOOTTYPE" == "legacy" ]; then
	mkdir /mnt/boot
	mount $BOOT /mnt/boot
else
	mkdir -p /mnt/boot/EFI
	mount $BOOT /mnt/boot/EFI
fi

#Base system install
echo "${green}[-] Installing base system ${reset}"
pacstrap /mnt base base-devel
genfstab -U -p /mnt >> /mnt/etc/fstab

#Create post-script
echo "${green}[-] Creating post-triggerd script ${reset}"
DAT=/mnt/root/data.dat
touch $DAT
echo "NAME=$NAME" >> $DAT
echo "USERNAME=$USERNAME" >> $DAT
echo "PARTITION=$HOMEDIR" >> $DAT
echo "BOOTTYPE=$BOOTTYPE" >> $DAT
INST=/mnt/root/install.sh
touch  $INST
chmod +x $INST
cat << 'EOF' >> $INST
#!/bin/bash
. /root/data.dat
pacman -S wget git --noconfirm
wget https://raw.githubusercontent.com/sutymuty/arch_installer/master/install_me_2.sh
chmod +x install_me_2.sh
./install_me_2.sh $NAME $USERNAME $PARTITION $BOOTTYPE
exit 0
EOF

#chrooting
echo "${green}[-] Starting the next step"\!"${reset}"
arch-chroot /mnt "/root/install.sh"

#delete old scripts
sleep 2
if [ -f /mnt/root/data.dat ]; then
	rm /mnt/root/data.dat
fi

if [ -f /mnt/install_me_2.sh ]; then
	rm /mnt/install_me_2.sh
fi

if [ -f /mnt/install.sh ]; then
	rm /mnt/root/install.sh
fi

echo "${green}[-] Umounting partitions ${reset}"
sleep 2
#umounting partitions
if [ "$BOOTTYPE" == "legacy" ]; then
	umount /mnt/boot
else
	umount /mnt/boot/EFI
fi
umount /mnt/
cryptsetup close cryptroot

#ending
echo "${green}[=] Installation finished, took $SECONDS seconds${reset}"
sleep 2
echo "${white}[?] Reboot your PC? (yes or no)${reset} "
read ANSWER
if [ "$ANSWER" == "yes" ]; then
  timer=10
  while [ $timer -ge 0 ]; do
    echo -ne "${red}Rebooting in $timer seconds (Press CTRL+C to cancel)! \r${reset}"
    sleep 1
    ((timer--))
  done
  `reboot`
else
	echo "${green}Well, byebye!${reset}"
fi

exit 0
