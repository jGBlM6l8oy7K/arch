#!/bin/bash
#Created by Bőhm Tamás
#www.github.com/BohmTamas/arch_installer/

#colors
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

#pacman
echo "${red}[-] Installing IT security things${reset}"
sudo pacman -S findmyhash whois mtr traceroute macchanger tor zmap torsocks proxychains-ng metasploit bettercap wireshark-gtk aircrack-ng hydra ncrack sqlmap tcpdump nmap httrack medusa nikto hashcat gdb john zaproxy net-tools --noconfirm

#aur	
echo "${red}[-] Installing IT security things from aur${reset}"
yay -S burpsuite theharvester-git crunch netdiscover ipscan whatweb crunch --noconfirm

sudo systemctl stop tor
sudo systemctl disable tor

echo "${red}[?] Shall we install beEF?(Y/n)${reset}"
read AWS
if [[ "$AWS" == "" || "$AWS" == "y" ]]; then
	DEST=$HOME/.beef
	sudo gem install xmlrpc
	sudo gem install bundle
	sudo mkdir $DEST
	sudo git clone https://github.com/beefproject/beef $DEST
	sudo chmod +x $DEST/install
	sudo $DEST/install
	sudo echo "gem \'xmlrpc\'" >> $DEST/Gemfile
	git clone https://github.com/bettercap/caplets $HOME/caplets/
fi

echo "${green}[-] Cloning exploit-db${reset}"
sudo git clone https://github.com/offensive-security/exploit-database.git $HOME/exploit-database

#social-engineer-toolkit
echo "${green}[-] Installing SET${reset}"
sudo git clone https://aur.archlinux.org/social-engineer-toolkit.git $HOME/social-engineer-toolkit/
cd $HOME/social-engineer-toolkit/
makepkg -Acs PKGBUILD
sudo pacman -U *.pkg.tar.xz 

#sqliv
echo "${green}[-] Installing sqliv${reset}"
sudo git clone https://github.com/Hadesy2k/sqliv.git $HOME/sqliv/
cd $HOME/sqliv
chmod +x setup.py
sudo python2 setup.py -i

#nessus
echo "${green}[-] Installing Nessus${reset}"
yay -S nessus --noconfirm
if [ $? -ne 0 ]; then
	echo "${red}["\!"] Something went wrong :\( ${reset}"
	sudo pacman -S rpmextract --noconfirm
	echo "${green} Do it by hand :D ${reset}"
fi
echo "${green}[=] Finished"\!"${reset}"
read -p "Press [ENTER] to exit..."

exit 0
