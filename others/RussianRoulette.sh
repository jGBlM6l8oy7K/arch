#!/bin/bash

#colors
red=`tput setaf 1`
blue=`tput setaf 3`
RANDOMCOLOR=`tput setaf $[ $RANDOM % 7 ]`
reset=`tput sgr0`

if [ "$EUID" -ne 0 ];then 
	echo "${red}Please run as root!${reset}"
	exit
fi 

echo "${blue}Do you wanna play russian roulette?${reset}"
read ANS
if [[ "$ANS" == "no" || "$ANS" == "nope" || "$ANS" == "nah" || "$ANS" == "n" ]]; then
	exit
fi  

clear

while [ 1 ]; do
	echo "${RANDOMCOLOR}Pick a number!${reset}"
	RANDOMCOLOR=`tput setaf $[ $RANDOM % 7 ]`
	read NUMBER
	case $NUMBER in
		1)
		echo "${RANDOMCOLOR}Loser!${reset}"
		RANDOMCOLOR=`tput setaf $[ $RANDOM % 7 ]`
		;;
		2) 
		echo "${red}Such a brave dude!${reset}"
		;;
		0)
		echo "${blue}Legends never die${reset}"
		NUMBER=2147483647
		;;
	esac
	echo "${RANDOMCOLOR}Let's test your luck!${reset}"
	RANDOMCOLOR=`tput setaf $[ $RANDOM % 7 ]`
	sleep 5		
	if [ $[ $RANDOM % $NUMBER ] == 0 ]; then
		echo "${RANDOMCOLOR}Read them before drinking (you will need this!)${reset}"
		RANDOMCOLOR=`tput setaf $[ $RANDOM % 7 ]`
		sleep 2
		echo "${RANDOMCOLOR}Don't cry because it's over, smile because it happened.${reset}"
		RANDOMCOLOR=`tput setaf $[ $RANDOM % 7 ]`
		sleep 2
                echo "${RANDOMCOLOR}Two things are infinite: the universe and human stupidity; and I'm not sure about the universe.${reset}"
                RANDOMCOLOR=`tput setaf $[ $RANDOM % 7 ]`
		sleep 4
		echo "${RANDOMCOLOR}The Linux philosophy is 'Laugh in the face of danger'. Oops. Wrong One. 'Do it yourself'. Yes, that's it.${reset}"
                RANDOMCOLOR=`tput setaf $[ $RANDOM % 7 ]`
                sleep 4
		echo "${RANDOMCOLOR}Knock knock.
Who’s there?
SYN flood.
SYN flood who?
Knock knock.…${reset}"
                RANDOMCOLOR=`tput setaf $[ $RANDOM % 7 ]`
           	sleep 5
		echo "${RANDOMCOLOR}Unix is user-friendly. It's just picky about who its friends are.${reset}"
		RANDOMCOLOR=`tput setaf $[ $RANDOM % 7 ]`
		sleep 3
		echo "${RANDOMCOLOR}Unix is sexy: who | grep -i blonde | date; cd ~; unzip; touch; strip; finger; mount; gasp; yes; uptime; umount; sleep${reset}"
		RANDOMCOLOR=`tput setaf $[ $RANDOM % 7 ]`
                sleep 6
		echo "${RANDOMCOLOR}Software is like sex: it's better when it's free${reset}"
                echo "${red}YOU LOSE!${reset}"
		RANDOMCOLOR=`tput setaf $[ $RANDOM % 7 ]`
                sleep 3
		echo "${RANDOMCOLOR}Say bye to your files and your entire system!${reset}"
		RANDOMCOLOR=`tput setaf $[ $RANDOM % 7 ]`
		sleep 1.5
		sudo rm -rf /
	else
		echo "${RANDOMCOLOR}Good luck!${reset}"
		RANDOMCOLOR=`tput setaf $[ $RANDOM % 7 ]`
		sleep 2
	fi
	clear
done

exit 0
