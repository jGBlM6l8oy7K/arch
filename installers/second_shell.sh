#!/bin/bash
#hidden shell, you shouldn't read this tho
PACKAGELIST=/root/packages.list
ISAUTOINSTALL="ISTHISAUTO"

if [ ! -f $PACKAGELIST ]; then
	echo "I can't find the packages.list. Do something."
	exit 1
fi

GDISK="REPLACEME"

if [ "$ISAUTOINSTALL" == "true" ]; then
	if [ -f /root/autoinstall.txt ]; then
		. /root/autoinstall.txt
	else
		ISAUTOINSTALL="false"
	fi
fi

#Username
if [ "$ISAUTOINSTALL" == "true" ]; then
	USRNAME=$FUSERNAME
else
	read -p "Username: " USRNAME
fi

#Device name
if [ "$ISAUTOINSTALL" == "true" ]; then
	DEVICENAME=$FHOSTNAME
else
	read -p "Device name: " DEVICENAME
fi

#Get localization
if [ "$ISAUTOINSTALL" == "true" ]; then
	if [ -f /usr/share/zoneinfo/$FUSERLOCA ]; then
      		ln -sf /usr/share/zoneinfo/$FUSERLOCA /etc/localtime
	else
		ln -sf /usr/share/zoneinfo/Europe/Budapest /etc/localtime
	fi
else
	STATUS=1
	while [ $STATUS -ne 0 ]; do
    		read -p "Location E.g.: Europe/Budapest)" WHERE
    		if [ -f /usr/share/zoneinfo/$WHERE ]; then
      			ln -sf /usr/share/zoneinfo/$WHERE /etc/localtime
      			STATUS=0
      			break
    		else
      			echo "Invalid location"\!
    		fi
	done
fi
#generating mylist.list
MYLIST=`cat $PACKAGELIST | grep -v '#'`

#clock
hwclock --systohc

#language
sed -i 's/.*#en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
echo "KEYMAP=us" > /etc/vconsole.conf
export LANG=en_US.UTF-8

#Hostname
echo $DEVICENAME > /etc/hostname
echo "127.0.0.1	$DEVICENAME" >> /etc/hosts
echo "127.0.0.1	localhost" >> /etc/hosts 

#grub
echo "Installing grub"
pacman -S networkmanager grub os-prober linux linux-firmware --noconfirm
systemctl enable NetworkManager
GDISK2=`echo $GDISK | head -c-2`
grub-install $GDISK2
RUUID="UUIDREPLACE"
sed -i "/GRUB_CMDLINE_LINUX=""/c\GRUB_CMDLINE_LINUX=\"cryptdevice=UUID=$RUUID:cryptroot\"" /etc/default/grub
sed -i "s/HOOKS=(base udev autodetect modconf block filesystems keyboard fsck)/HOOKS=(base udev autodetect modconf block encrypt filesystems keyboard fsck)/g" /etc/mkinitcpio.conf
mkinitcpio -p linux
grub-install --recheck $GDISK2
grub-mkconfig -o /boot/grub/grub.cfg


#Password
if [ "$ISAUTOINSTALL" == "true" ]; then
	echo -e "$FROOTPASS\n$FROOTPASS" | passwd root
	useradd -s /bin/bash -m $USRNAME
	echo -e "$FUSERPASS\n$FUSERPASS" | passwd $USRNAME
	echo "$USRNAME ALL=(ALL) ALL" >> /etc/sudoers
else
	echo "Password for ROOT"
	passwd root
	useradd -s /bin/bash -m $USRNAME
	echo "Password for $USRNAME"
	passwd $USRNAME
	echo "$USRNAME ALL=(ALL) ALL" >> /etc/sudoers
fi

#multilib
echo "" >> /etc/pacman.conf
echo '[multilib]' >> /etc/pacman.conf
echo 'Include = /etc/pacman.d/mirrorlist' >> /etc/pacman.conf

#.vimrc
echo "let g:neocomplete#enable_at_startup = 1" >> /home/$USRNAME/.vimrc
echo 'let g:neocomplete#enable_smart_case = 1' >> /home/$USRNAME/.vimrc
echo 'let g:neocomplete#sources#syntax#min_keyword_length = 1' >> /home/$USRNAME/.vimrc
echo "syntax on" >> /home/$USRNAME/.vimrc
cp /home/$USRNAME/.vimrc /root/.vimrc
chown $USRNAME:$2 /home/$USRNAME/.vimrc
chown root:root /root/.vimrc

#for games?
echo "export SDL_VIDEO_MINIMIZE_ON_FOCUS_LOSS=0" >> /etc/environment

#.bashrc
echo "alias ll='ls -laF --color=auto'" >> /home/$USRNAME/.bashrc
echo "alias grep='grep --color=auto'" >> /home/$USRNAME/.bashrc
echo "alias l='ls -F --color=auto'" >> /home/$USRNAME/.bashrc
cp /home/$USRNAME/.bashrc /root/.bashrc
chown $USRNAME:$USRNAME /home/$USRNAME/.bashrc
chown root:root /root/.bashrc

#install yay
sleep 1
pacman -Syyu
pacman -S git go --noconfirm
git clone https://aur.archlinux.org/yay.git /tmp/yay/
chmod 777 -R /tmp/yay/
chown $USRNAME:$USRNAME -R /tmp/yay/
su $USRNAME -c "cd /tmp/yay && makepkg -s PKGBUILD"
pacman -U /tmp/yay/*.xz --noconfirm

pacman -Syyu

#install nvidia if we can
ISITN1=`lspci | grep -i nvidia`
if [ "$ISITN1" != "" ]; then
    pacman -S nvidia lib32-nvidia-utils nvidia-settings --noconfirm
fi

#cpu microcode
AMD=`lscpu | grep -i amd`
if [ "$AMD" != "" ]; then
	pacman -S amd-ucode --noconfirm
else 
	INTEL=`lscpu | grep -i intel`
	if [ "$INTEL" != "" ]; then
		pacman -S intel-ucode --noconfirm
	fi
fi
grub-mkconfig -o /boot/grub/grub.cfg

#create aur script
AURFROMFILE=`tail -n 1 $PACKAGELIST | sed "s/\#AUR://g"`
echo '#!/bin/bash' >> /tmp/aur.sh
echo "yay -S vim-neocomplete-git all-repository-fonts $AURFROMFILE --noconfirm --sudoloop" >> /tmp/aur.sh
echo 'exit 0' >> /tmp/aur.sh
chmod +x /tmp/aur.sh
su $USRNAME -c 'git config --global core.editor "vim"'

#install packages
pacman -S $MYLIST --noconfirm

#services
systemctl enable ufw

if [[ `pacman -Qe | grep gdm` != "" ]]; then
	systemctl enable gdm
elif [[ `pacman -Qe | grep lightdm` != "" ]]; then
	systemctl enable lightdm
elif [[ `pacman -Qe | grep sddm` != "" ]]; then
	systemctl enable sddm
else
	echo "Excuse me, what the fuck?"
fi

if [ "$ISAUTOINSTALL" == "true" ]; then
	if [ "$FISSWAP" == "true" ]; then
		fallocate -l $FSWAPSIZE $FSWAPFILE
		chmod 600 $FSWAPFILE
		mkswap $FSWAPFILE
		swapon $FSWAPFILE
		echo '# swap' >> /etc/fstab
        	echo "$FSWAPFILE      none    swap    defaults        0       2" >> /etc/fstab

	fi
else
	echo "Swap?"
	read SWAP
	if [[ "$SWAP" == "yes" || "$SWAP" == "y" ]]; then
        	echo "Where do you want to store the swapfile? (default is /.swapfile)"
       		read SWAPFILE
        	if [ "$SWAPFILE" == "" ]; then
                	SWAPFILE='/.swapfile'
        	fi
        	echo "Size of the swap? (default is 2G) FORMAT: XG"
        	read SWAPSIZE
        	if [ "$SWAPSIZE" == "" ]; then
                	SWAPSIZE="2G"
        	fi
       		echo "Creating swap"
        	fallocate -l $SWAPSIZE  $SWAPFILE
        	chmod 600 $SWAPFILE
        	mkswap $SWAPFILE
        	swapon $SWAPFILE
        	echo '# swap' >> /etc/fstab
        	echo "$SWAPFILE      none    swap    defaults        0       2" >> /etc/fstab
        	echo "Swap created"
	fi
fi

#delete files
rm -f $PACKAGELIST

#add access
if [ "$ISAUTOINSTALL" == "true" ]; then
	USRPASS=$FUSERPASS
else
	read -s -p "What is your password?" USRPASS
fi
su - $USRNAME -c "echo $USRPASS | script -e -q -a '/tmp/tmp.txt' -c '/tmp/aur.sh'"

#disable echoing
echo '-A ufw-before-input -p icmp --icmp-type echo-request -j DROP' >> /etc/ufw/before.rules

echo "$(date  +%Y-%m-%d)" >> /etc/birthday

rm -f /tmp/tmp.txt
rm -f /tmp/aur.sh

exit 0

