#!/bin/bash
#OP: https://www.reddit.com/r/archlinux/comments/9gaixv/creating_a_hardened_arch_linux_installation_with/
#Variables
 #usb
USBNAME=""		#the path to the USB (will be the /boot) device, user input
EFIPART=""		#efi partiton name on the USB device, user input
USBLINUX=""		#the linux partiton ont the USB
USBLINUXUUID=""		#the uuid of the usb linux partition

 #Disk
DISKNAME=""		#the name of the disk, user input
ROOTPART=""		#name of the root partiton
HEADERPART=""       	#name of the header partition
ROOTUUID=""		#the UUID of the root partition


 #other
CONFIRMATION=""     	#can be y/yes, user input

#Disclamer
echo '   #     #                                                                                       
    #   #   ####  #    #     ####  #    #  ####  #    # #      #####     #    #  ####  #####     
     # #   #    # #    #    #      #    # #    # #    # #      #    #    ##   # #    #   #       
      #    #    # #    #     ####  ###### #    # #    # #      #    #    # #  # #    #   #       
      #    #    # #    #         # #    # #    # #    # #      #    #    #  # # #    #   #    
      #    #    # #    #    #    # #    # #    # #    # #      #    #    #   ## #    #   #   ### 
      #     ####   ####      ####  #    #  ####   ####  ###### #####     #    #  ####    #   ### '

#if you remove this, its gonna run lol.
exit 0

#just to be sure remove this too
exit 0

#remove that exit 0 to be really confident
read -p 'This installer may break your entire system so create backup from everything! GLHF'
exit 0

#list devices
echo 'This is your disks now'
lsblk

#getting usb device
read -p 'Insert the USB (at least 2GB) and then press enter'

while true; do
	lsblk
	read -p 'What is your USB device? ' USBNAME
		ls $USBNAME >> /dev/null
		if [[ $? -eq 0 && "$(echo ${USBNAME: -1})" != [0-9] ]]; then
			read -p "Are you sure your USB device is $USBNAME? " CONFIRMATION
			if [[ "$CONFIRMATION" == "yes" || "$CONFIRMATION" == "y" ]]; then
				break;
			fi
		else
			echo "Invalid device name: $USBNAME "\!
		fi
done

#delete all the data from the usb
read -p "Shall we erasing the USB? it may takes a while. " CONFIRMATION
if [[ "$CONFIRMATION" == "yes" || "$CONFIRMATION" == "y" ]]; then
	echo "Erasing your data on your USB device: $USBNAME, please wait."
	dd if=/dev/urandom of=$USBNAME status=progress
fi

#partitioning
while true; do 
	lsblk
	read -p 'Which device is your disk? ' DISKNAME
	ls $DISKNAME >> /dev/null
	if [[ $? -eq 0 && "$(echo ${DISKNAME: -1})" != [0-9] ]]; then
		read -p "Are you sure your device is $DISKNAME? " CONFIRMATION
		if [[ "$CONFIRMATION" == "yes" || "$CONFIRMATION" == "y" ]]; then
			break;
		fi
	else
		echo "Invalid disk name (should be like /dev/sdX)"
	fi
done

echo "You will need 2 partitions: 1->the / partiton (should be almost the entire disk) and a header partition (should be 500M)"
read -p "Press enter to start partitioning"
cfdisk $DISKNAME

#getting the partitions
while true; do
	lsblk $DISKNAME
	read -p 'What is the name of the root partiton? ' ROOTPART
	read -p 'What is the name of the header partition? ' HEADERPART
	ls $ROOTPART >> /dev/null
	if [ $? -eq 0  ]; then
		ls $HEADERPART >> /dev/null
		if [ $? -eq 0 ]; then
			read -p "Are you sure? root=$ROOTPART and header=$HEADERPART " CONFIRMATION
			if [[ "$CONFIRMATION" == "yes" || "$CONFIRMATION" == "y" ]]; then
				break;
			fi
		else
			echo "Invalid header partition: $HEADERPART"
		fi
	else
		echo "Invalid root partiton: $ROOTPART"
	fi
done

#erasing the partitions
read -p "Shall we erase the partitions? " CONFIRMATION
if [[ "$CONFIRMATION" == "yes" || "$CONFIRMATION" == "y" ]]; then
	echo "Erasing the data from the partitions"
	dd if=/dev/urandom of=$ROOTPART status=progress
	dd if=/dev/urandom of=$HEADERPART status=progress
fi

#Formating the header partition and mounting
mkfs.ext4 $HEADERPART
mkdir /mnt/headerstore
mount $HEADERPART /mnt/headerstore

#creating headerfile
cd /mnt/headerstore && dd if=/dev/zero of=header.img bs=4M count=1 conv=notrunc

#root partition encrypting
cryptsetup luksFormat --type luks2 $ROOTPART --align-payload 8192 --header /mnt/headerstore/header.img
cryptsetup open --header=/mnt/headerstore/header.img $ROOTPART arch

#USB now

while true; do
	echo "You will need 2 partitons on the USB: "
	echo "  1) 512M EFI Partiton"
	echo "  2) Rest in Linux filesystem"
	read -p "Press enter to start"
	cfdisk $USBNAME
	lsblk
	read -p 'Could you succeed? ' CONFIRMATION
	if [[ "$CONFIRMATION" == "yes" ||  "$CONFIRMATION" == "y" ]]; then
		break;
	fi
done

while true; do
	#EFI
	lsblk
	read -p "What is the name of the EFI partiton on the USB device? " EFIPART
	ls $EFIPART >> /dev/null
	if [ $? -ne 0 ]; then
		echo "Invalid EFI partition: $EFIPART"\!
		continue
	fi
	
	#linux part
	read -p "What is the name of the Linux partition on the USB device? " USBLINUX
	ls $USBLINUX >> /dev/null
	if [ $? -ne 0 ]; then
		echo "Invalid linux partition on UBS: $USBLINUX"\!
		continue
	fi
	read -p "Are you sure EFI partition is $EFIPART and the other is $USBLINUX? " CONFIRMATION
	if [[ "$CONFIRMATION" == "yes" || "$CONFIRMATION" == "y" ]]; then
		break
	fi
done

#formatting and encrypting
mkfs.vfat -F 32 $EFIPART
cryptsetup luksFormat $USBLINUX
cryptsetup open $USBLINUX cryptboot

#mounting
mkdir /mnt/cryptboot
mkfs.ext4 /dev/mapper/cryptboot
mount /dev/mapper/cryptboot /mnt/cryptboot

#cp the image
cp /mnt/headerstore/header.img /mnt/cryptboot/

umount /mnt/headerstore
read -p "Shall we erase the USB again? " CONFIRMATION
if [[ "$CONFIRMATION" == "yes" || "$CONFIRMATION" == "y" ]]; then
	dd if=/dev/urandom of=$HEADERPART status=progress
fi

#creating LVM
pvcreate /dev/mapper/arch
vgcreate archlinux /dev/mapper/arch

#reading the size of the root partition
lsblk 
lvcreate -l 100%FREE archlinux -n root
mkfs.ext4 /dev/archlinux/root

#umounting and deleting
umount -l /mnt/*
rm -r /mnt/*

#mount the arch in the right way
mount /dev/archlinux/root /mnt
mkdir /mnt/boot
mkdir /mnt/efi

mount /dev/mapper/cryptboot /mnt/boot
mount $EFIPART /mnt/efi

# insallating 
pacstrap /mnt base base-devel vim linux-hardened grub efibootmgr
genfstab -U /mnt >> /mnt/etc/fstab

# getting the post install script
wget https://gitlab.com/sutymuty/arch/blob/master/installers/hardened_install_second_shell.sh
chmod +x hardened_install_second_shell.sh

# setting up the post install script
ROOTUUID=`blkid $ROOTPART | grep UUID=\" | awk -F '"' '{print $2}'`
USBLINUXUUID=`blkid $USBLINUX | grep UUID=\" | awk -F '"' '{print $2}'`

sed -i "s|REPLACEME1|$ROOTUUID|g" hardened_install_second_shell.sh
sed -i "s|REPLACEME2|$USBLINUXUUID|g" hardened_install_second_shell.sh

read -p "Do you want to edit/see the second script? " CONFIRMATION
if [[ "$CONFIRMATION" == "yes" || "$CONFIRMATION" == "y" ]]; then
	vim hardened_install_second_shell.sh
fi

cp hardened_install_second_shell.sh /mnt/root/hardened_install_second_shell.sh

#chrooting
arch-chroot /mnt/ "/root/hardened_install_second_shell.sh"

#after 
sleep 5
rm -f /mnt/root/hardened_install_second_shell.sh
umount -l /mnt/*

exit 0
